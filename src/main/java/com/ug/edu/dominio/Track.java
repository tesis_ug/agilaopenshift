package com.ug.edu.dominio;

@lombok.Data
public class Track {
    private Double accuracyEnd;
    private Double accuracyLastStepAltitude;
    private Double accuracyLastStepDistance;
    private Double accuracyStart;
    private Long altitudeDown;
    private Double altitudeEnd;
    private AltitudeFilter altitudeFilter;
    private Double altitudeInProgress;
    private Long altitudeLastStepAltitude;
    private Long altitudeStart;
    private Long altitudeUp;
    private String description;
    private Double distance;
    private Double distanceInProgress;
    private Long distanceLastAltitude;
    private Long duration;
    private Long durationMoving;
    private Long egmAltitudeCorrectionEnd;
    private Long egmAltitudeCorrectionStart;
    private Long id;
    private Boolean isSelected;
    private Double latitudeEnd;
    private Double latitudeLastStepDistance;
    private Double latitudeMax;
    private Double latitudeMin;
    private Double latitudeStart;
    private Double longitudeEnd;
    private Double longitudeLastStepDistance;
    private Double longitudeMax;
    private Double longitudeMin;
    private Double longitudeStart;
    private String name;
    private Long numberOfLocations;
    private Long numberOfPlacemarks;
    private Double speedAverage;
    private Double speedAverageMoving;
    private Long speedEnd;
    private Double speedMax;
    private Long speedStart;
    private Long timeEnd;
    private Long timeLastFix;
    private Long timeStart;
    private Long type;
    private Long validMap;
}
