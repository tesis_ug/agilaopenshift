package com.ug.edu.dominio;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Request {
	private LocalDateTime startDate;
	private LocalDateTime endDate;
}
