package com.ug.edu.dominio;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.MongoId;

@lombok.Data
public class Tracking {
	@MongoId
	private BigDecimal requestId;
    private List<PlacemarkList> placemarkList;
    private List<PlacemarkList> recorrido;
    private Track track;
    private String usuario;
}
