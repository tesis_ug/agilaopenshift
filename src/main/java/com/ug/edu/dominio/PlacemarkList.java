package com.ug.edu.dominio;

@lombok.Data
public class PlacemarkList {
    private Long altitudeEGM96Correction;
    private String description;
    private Location location;
    private Double mAltitude;
    private Long mBearing;
    private Long mBearingAccuracyDegrees;
    private Long mElapsedRealtimeUncertaintyNanos;
    private Long mHorizontalAccuracyMeters;
    private Double mLatitude;
    private Double mLongitude;
    private Double mSpeed;
    private Long mSpeedAccuracyMetersPerSecond;
    private Long mVerticalAccuracyMeters;
    private Long numberOfSatellites;
    private Long numberOfSatellitesUsedInFix;
}
