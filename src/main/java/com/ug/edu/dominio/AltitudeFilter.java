package com.ug.edu.dominio;

@lombok.Data
public class AltitudeFilter {
    private Long maxAcceleration;
    private Long stabilizationTime;
    private Long goodTime;
    private Long newAltitude;
    private Long newTime;
    private Long newVerticalSpeed;
    private Long prevAltitude;
    private Long prevTime;
    private Long prevVerticalSpeed;
    private Long timeInterval;
    private Long verticalAcceleration;
}
