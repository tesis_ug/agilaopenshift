package com.ug.edu.dominio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


import org.springframework.data.mongodb.core.mapping.MongoId;

import lombok.Data;

@Data
public class CreditLevelResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3851076601504008251L;

	@MongoId
	private BigDecimal requestId;
	private String messageDescription;
	private String originalHostName;
	private String hostName;
	private String address;
	private LocalDateTime startDate;
    private Tracking tracking;
}
