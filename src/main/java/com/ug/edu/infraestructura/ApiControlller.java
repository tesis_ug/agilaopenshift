package com.ug.edu.infraestructura;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.google.gson.Gson;
import com.ug.edu.RepositoryCreditLevel;
import com.ug.edu.dominio.CreditLevelResponse;
import com.ug.edu.dominio.Request;
import com.ug.edu.dominio.Tracking;

import lombok.extern.java.Log;

@Log
@RestController
@RequestMapping(value = "/ug")
public class ApiControlller {
	
	@Autowired
	RepositoryCreditLevel creditLevel;
	
	@PostMapping(value = "/CreditRequest")
	public DeferredResult < ResponseEntity < String >> asynchronousRequestProcessing(@RequestBody(required = true) String requestString) {

		ThreadContext.put("sid", UUID.randomUUID().toString());
		
		try {
			CreditLevelResponse creditLevelResponse = new CreditLevelResponse();
			creditLevelResponse.setRequestId(new BigDecimal(System.currentTimeMillis()));
			log.info(requestString);
			InetAddress direccion = InetAddress.getLocalHost();
			creditLevelResponse.setAddress(direccion.getHostAddress());
			creditLevelResponse.setMessageDescription(requestString);
			creditLevelResponse.setHostName(direccion.getHostName());
			creditLevelResponse.setStartDate(LocalDateTime.now());
			final DeferredResult < ResponseEntity < String >> deferredResult = new DeferredResult < > ();
			creditLevel.save(creditLevelResponse);
			deferredResult.setResult(new ResponseEntity<>(requestString, HttpStatus.OK));
			return deferredResult;
		} catch (Exception e) {
			return null;
		}
	}
	
	@PostMapping(value = "/getCreditRequest")
	public DeferredResult < ResponseEntity < List<CreditLevelResponse> >> getRequestProcessing(@RequestBody(required = true) Request request) {

		ThreadContext.put("sid", UUID.randomUUID().toString());
		
		try {
			final DeferredResult < ResponseEntity < List<CreditLevelResponse> >> deferredResult = new DeferredResult < > ();

			 List<CreditLevelResponse>  creditLevelResponses =  creditLevel.findByStartDateBetween(request.getStartDate(), request.getEndDate());
			 creditLevelResponses.stream().parallel().forEach(x-> {
				 x.setTracking(new Gson().fromJson(x.getMessageDescription(), Tracking.class));
				 x.setMessageDescription("");
			 });
			deferredResult.setResult(new ResponseEntity<>(creditLevelResponses, HttpStatus.OK));
			return deferredResult;
		} catch (Exception e) {
			return null;
		}
	}

}
