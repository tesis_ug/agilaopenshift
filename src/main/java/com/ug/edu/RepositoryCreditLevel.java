package com.ug.edu;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ug.edu.dominio.CreditLevelResponse;

public interface RepositoryCreditLevel extends MongoRepository<CreditLevelResponse, BigDecimal> {
	
	public List<CreditLevelResponse> findByStartDateBetween(LocalDateTime startDate,LocalDateTime endDate);

}
